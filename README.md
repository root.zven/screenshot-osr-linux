1) Скрипту требуются следующие пакеты на примере Arch Linux

Программа для снятия скриншотов в файл:
`sudo pacman -S gnome-screenshot`

imagemagick для обработки изображения
`sudo pacman -S imagemagick`

Плагин к imagemagick для обработки снимка текста
http://www.fmwconcepts.com/imagemagick/textcleaner/index.php
(скачать и положить рядом со скриптом)

Доступ к буферу обмена X11 из командной строки
`sudo pacman -S xclip`

Непосредственно движок распознавания текста https://github.com/tesseract-ocr/tesseract с языковыми пакетами
`sudo pacman -S 	tesseract tesseract-data-eng tesseract-data-eng`

2) Сделать скрипт исполняемым

3) Забиндить клавиатурную комбинацию для быстрого вызова, например `Ctrl+PrtSc`

