!/bin/sh
# Dependencies: tesseract-ocr imagemagick gnome-screenshot xclip and http://www.fmwconcepts.com/imagemagick/textcleaner/index.php

#откуда запущен скрипт
SCR=`dirname $0`

#временная папка в которой будем обрабатывать картинку
OUT="/tmp"


#магия
sleep 1


#делаем скриншот
gnome-screenshot --area -f $OUT/ss.tif

#чистим текст от загрязнений
$SCR/textcleaner -g -e stretch -f 25 -o 2 $OUT/ss.tif $OUT/ss.tif

#увеличивыем размер картинки(магия)
convert $OUT/ss.tif -sample 377% $OUT/ss.tif

#распознаём буквы
tesseract $OUT/ss.tif $OUT/ss -l eng+rus --psm 6

#отправляем в буфер обменатекст
cat $OUT/ss.txt | xclip -selection clipboard

#отправляем сообщение службе уведомлений, что всё готово
notify-send 'OSR Готово!' 'Текст в буфере обмена'


# Параметры textcleaner
#-g - cсделать серым
#-e - нормализация усиление белого (none, stretch, or normalize)
#-f - фильтр очистки фона - целое число> 0; по умолчанию = 15
#-o - уменьшение шума в процентах - по умолчанию 5
#-t - порог сглаживания текста. Значения являются целыми числами от 0 до 100 (визуально утолщает текст)(ошибка?)
#-a - альтернативное сглаживания текста floats>=0; default=0 (ошибка?)
#-u - выравнивание строк(поворот изображения)
#-s - резкость nominal about 1; default=0 - дробное число может быть
